/**
 * Reads a CSV file, extracts urls, scrapes Price & Title
 * Note: This is for test.csv file only, It considers a row which has a url in it 
 */

const fs = require("fs");
const rl = require("readline");
const { scraper } = require('./tools/scraper');
const { writeToFile } = require('./tools/writeToFile');

// stream the file
const reader = rl.createInterface({
    input: fs.createReadStream("./testFiles/test.csv")
});

// read row by row
const arr = [];
reader.on("line", (row) => {
    arr.push(row);
});

// on close, call the scraper
reader.on("close", () => {
    scraper(arr).then((result) => {
        console.log(result);
        writeToFile(result);
    }).catch((err) => {
        console.log(err);
    });
});
