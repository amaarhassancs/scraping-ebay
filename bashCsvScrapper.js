/**
 * bash script: cat ./testFiles/test.csv | node bashCsvScrapper.js
 */
const { scraper } = require('./tools/scraper');
const { writeToFile } = require('./tools/writeToFile');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    let csvArray = inputString.trim().split('\n').map(string => {
        return string.trim();
    });

    scraper(csvArray).then((result) => {
        console.log(result);
        writeToFile(result);
    }).catch((err) => {
        console.log(err);
    });
});
