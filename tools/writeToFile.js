/**
 * Save the given data of array into a text file on hard disk
 * Note: This file/method is for particular scraping only.
 * It accepts price and title as array of objects and saves them in a csv file
 */

const fs = require('fs');

const writeToFile = (data) => {
    console.log('\n \t Writing data into file scraped.csv');

    const writeStream = fs.createWriteStream("./scraped.csv");

    const headers = ["title", "price"];

    writeStream.write(`${headers.join()}\n`);

    for (let object of data) {
        writeStream.write(`${object.title},${object.price}\n`);
    }

    console.log('\n \t Writing to file Completed')
    writeStream.end();
}

module.exports = {
    writeToFile
}