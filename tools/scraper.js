/**
 * This file would scrape urls given in the urls.
 * It will use puppeteer to open browser and then use DOM to extract
 * the $Price and #Title of the given ebay urls
 */

const puppeteer = require('puppeteer');

let scraper = async (urls) => {
    const results = [];

    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();

    for (let i = 0; i < urls.length; i++) {
        let url = urls[i];
        console.log(`...scraping ${i + 1} of ${urls.length} urls`);
        await page.goto(url);

        // Scrape
        const result = await page.evaluate(() => {
            let title = document.querySelector('#itemTitle').innerText.split('\n')[1];  // doing this to clean text
            let price = document.querySelector('#prcIsum').innerText;
            return {
                title,
                price
            }
        });

        // push to results
        results.push(result);
    };
    // close browser and exit
    browser.close();
    return results;
};

module.exports = {
    scraper
}