# scraping-ebay
npm i
Scraping ebay urls for price and title. It uses a combination of following inputs.
 1. Scrape from urls directly (urlscrapper),
 2. Scrape from a separate csv file (csvscrapper), 
 3. Scrape from input from bash script (bashCsvScrapper)

 1. node urlscrapper.js
 2. node csvscrapper.js
 3. cat ./testFiles/test.csv | node bashCsvScrapper.js 